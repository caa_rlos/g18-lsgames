<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;

class Product extends Model {

    protected $table = 'product';

    public static function addProduct($title, $description, $price, $stock, $expiration_date, $new_file, $user_id) {

        $filename_path = $new_file->getClientOriginalName();
        $filename_path = str_replace(' ', '_', $filename_path);
        $destination_path = public_path().'/images/products/';

        $friendly_url = Product::format_uri($title, '_');

        $new_product = new Product();

        $new_product->title = $title;
        $new_product->description = $description;
        $new_product->price = $price;
        $new_product->stock = $stock;
        $new_product->user_id = $user_id;
        $new_product->expiration_date = $expiration_date;
        $new_product->filename_path = $new_product->id . '-' . $filename_path;
        $new_product->filename = $new_file->getClientOriginalName();
        $new_product->friendly_url = $friendly_url;
        $new_product->old_url = $friendly_url;
        $new_product->created_at = Carbon::now();
        $new_product->updated_at = Carbon::now();

        $new_product->save();

        $filename_path = $new_product->id . '-' . $filename_path;
        $id = $new_product->id;
        $new_product->filename_path = $filename_path;
        $new_product->save();

        //$new_file->move($destination_path, $filename_path);


        Product::resizeImage($new_file, $id, $destination_path, '0');
        Product::resizeImage($new_file, $id, $destination_path, '1');
        Product::resizeImage($new_file, $id, $destination_path, '2');

    }

    public static function resizeImage($image, $id, $destination_path, $type) {

        $filename = $type . '-'.$id.'.jpg';

        $path = $destination_path.$filename;
        if ($type == '0'){
            Image::make($image->getRealPath())->save($path);
        }elseif($type == '1'){
            Image::make($image->getRealPath())->resize(400,300)->save($path);
        }elseif($type == '2') {
            Image::make($image->getRealPath())->resize(100,100)->save($path);
        }

    }

    public static function getExpirationDate($product_id) {

        $date = Product::select('expiration_date')
            ->where('id', '=', $product_id)
            ->first();

        $today = Carbon::now()->toDateString();

        if($date->expiration_date > $today){
            //future date

            $time = explode('-', $date->expiration_date);
            $future_date = Carbon::createFromDate($time[0], $time[1], $time[2]);
            $time_now = explode('-', $today);
            $today_date = Carbon::createFromDate($time_now[0], $time_now[1], $time_now[2]);
            $days = $future_date->diffInDays($today_date);

            return $days;
        }else {
            return 0;
        }
    }

    public static function getStock($product_id) {

        $stock = Product::select('stock')
            ->where('id', '=', $product_id)
            ->first();

        if ($stock->stock > 0 ) {
            return true;
        }else {
            return false;
        }
    }

    public static function getProductInfo($product_id) {

        $product = Product::select('*')
            ->where('id', '=', $product_id)
            ->first();

        return $product;
    }

    public static function getSellerInfo($product_id) {

        $user_id =Product::select('user_id')
            ->where('id', '=', $product_id)
            ->first();


        $info = User::select('id', 'name', 'success_factor')
            ->where('id', '=', $user_id->user_id)
            ->first();

        return $info;
    }

    public static function getAllProductsFromUser($user_id) {

        $list = Product::select('id','title', 'friendly_url')->
            where('user_id', '=', $user_id)
            ->simplePaginate(10);

            return $list;
    }

    public static function deleteProduct ($product_id) {

        Product::where('id', '=', $product_id)
            ->delete();
    }

    public static function updateProduct ($product_id, $input) {


        $friendly_url = Product::format_uri($input['title'], '_');

        $old_url = Product::where('id', '=', $product_id)
            ->select('friendly_url')
            ->first();



        Product::where('id', '=', $product_id)
            ->update(array(
                'title' => $input['title'],
                'description' => $input['description'],
                'price' => $input['price'],
                'stock' => $input['stock'],
                'friendly_url' => $friendly_url,
                'old_url' => $old_url->friendly_url,
                'expiration_date' => $input['expiration_date'],
                'updated_at' => Carbon::now()
            ));

        $filename_path = $input['image']->getClientOriginalName();
        $filename_path = str_replace(' ', '_', $filename_path);
        $destination_path = public_path().'/images/products/';
        Product::resizeImage($input['image'], $product_id, $destination_path, '0');
        Product::resizeImage($input['image'], $product_id, $destination_path, '1');
        Product::resizeImage($input['image'], $product_id, $destination_path, '2');

    }

    public static function getAllProducts($search_text, $user_id) {

        $now =Carbon::now()->toDateTimeString();


        if ($search_text == ""){
            $query_search_text = '1=1';
        }else {
            $query_search_text = 'title LIKE "%'.$search_text.'%"';

        }

        $products = DB::table('product as p')
            ->join('user as u', 'u.id', '=', 'p.user_id')
            ->where('stock', '>', '0')
            ->whereRaw($query_search_text)
            ->where('expiration_date', '>', $now)
            ->where('user_id', '!=', $user_id)
            ->select('p.id', 'p.title', 'p.user_id', 'p.friendly_url', 'u.name', 'u.success_factor')
            ->simplePaginate(10);

        return $products;
    }

    public static function decreaseStock($product_id) {

        $stock = Product::select('stock')
            ->where('id', '=', $product_id)
            ->first();

        $stock = $stock->stock -1;



        Product::where('id', '=', $product_id)
            ->update(array('stock' => $stock));

    }

    public static function setUserPurchase ($user_id, $product_id, $seller_id) {

        DB::table('purchases')
            ->insert(array(
                'user_id' => $user_id,
                'product_id' => $product_id,
                'seller_id' => $seller_id
            ));
    }

    public static function getMyPurchases ($user_id) {

        $table = DB::table('purchases as pr')
            ->join('product as p', 'p.id', '=', 'pr.product_id')
            ->join('user as u', 'u.id', '=', 'p.user_id')
            ->where('pr.user_id', '=', $user_id)
            ->select('p.id','p.title', 'p.price', 'u.name', 'u.id as user_id','p.friendly_url')
            ->get();

        return $table;

    }

    public static function getLastProduct() {

        $product  = Product::select('*')
            ->orderBy('id', 'desc')
            ->first();

        return $product;
    }

    public static function getFiveLastProducts () {

        $product  = Product::select('id', 'friendly_url', 'title', 'description', 'expiration_date', 'price' )
            ->orderBy('visitors', 'desc')
            ->take(5)
            ->get();

        return $product;
    }

    public static function increaseOneVisitor($product_id) {

        $visitor = Product::select('visitors')
            ->where('id', '=', $product_id)
            ->first();
        $visitor = $visitor->visitors +1;
        Product::where('id', '=', $product_id)
            ->update(array(
                'visitors' => $visitor

            ));
    }

    public static function getPopularProducts () {

        $product = Product::select('id', 'title', 'description', 'expiration_date', 'visitors', 'friendly_url')
            ->orderBy('visitors', 'desc')
            ->simplePaginate(10);

        return $product;
    }

    public static function getTotalVisitors () {

        $visitors = Product::select('visitors')
            ->get();

        return $visitors;
    }

    public static function getNumberOfSellings($product_id) {

        $sellings =  DB::table('purchases')
            ->select('id')
            ->where('product_id', '=', $product_id)
            ->count();

        return $sellings;
    }

    public static function format_uri( $string, $separator = '-' )
    {
        $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
        $special_cases = array( '&' => 'and', "'" => '');
        $string = mb_strtolower( trim( $string ), 'UTF-8' );
        $string = str_replace( array_keys($special_cases), array_values( $special_cases), $string );
        $string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
        $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
        $string = preg_replace("/[$separator]+/u", "$separator", $string);
        return $string;
    }

    public static function getIdByFriendlyURL($friendly_url) {

        $product_id = Product::select('id')
            ->where('friendly_url', '=', $friendly_url)
            ->orWhere ('old_url', '=', $friendly_url)
            ->first();

        if (!is_null($product_id)) {


        }

        return $product_id->id;
    }


    // 301
    public static function checkOldUrl ($friendly_url) {

        $old_url = Product::where('old_url', '=', $friendly_url)
            ->select('old_url')
            ->first();

        $product =  Product::where('friendly_url', '=', $friendly_url)
            ->orWhere('old_url', '=', $friendly_url)
            ->select('friendly_url', 'old_url')
            ->first();

        if ($friendly_url == $product->friendly_url) {
            //esta be
            return true;
        }

        if (!is_null($old_url)) {
            Header('HTTP/1.1 301 Moved Permanently');
            Header('Location: ' . 'http://lsgames.local/product/'.$product->friendly_url);
            exit;

        }


    }

    public static function getURLbyOld ($url) {

        $friendly_url = Product::where('old_url', '=', $url)
            ->select('friendly_url')
            ->first();

        return $friendly_url['friendly_url'];
    }

    public static function changeDateFormat($date) {

        $time = explode('/', $date);

        $expiration_date = $time[2].'-'.$time[0].'-'.$time[1];

        return $expiration_date;
    }

    public static function checkURl($url) {
        $products = Product::select('old_url', 'friendly_url')
            ->get();

        $correct = false;

        foreach ($products as $product) {
            if ($product->friendly_url == $url or $product->old_url == $url) {

                $correct = true;
            }


        }
        return $correct;


    }
}