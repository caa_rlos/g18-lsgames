<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;

class ProductController extends Controller {

    public function add() {

        set_time_limit(0);
        ini_set('memory_limit', '20000M');

        $manager = new ImageManager(array('driver' => 'imagick'));
        $input = Request::all();
        $user_id = Session::get('user_id');
        $credits = Session::get('credits');
        $error_credits = false;
        $error_description = false;
        $image_path = "";
        $image_uploaded = false;
        $error_size = false;
        $error_extension = false;
        $extension = "";
        $error_date = false;
        if (is_null($user_id)) {
            return view ('error.403');
        }
        $today = Carbon::now()->toDateString();
        $rules = array(
            'title' => array('required', 'max:50'),
            'description' => array('required'),
            'price' => array('required','numeric', 'min:0'),
            'stock' => array('required','numeric', 'min:1', 'integer'),
            'expiration_date' => array('required','date',),
            'image' => array('required')
        );
        $messages = array(
            'required' => 'Este campo es obligatorio',
            'min' => 'El precio debe ser mayor que 0€',
            'max' => 'El título no puede superar los 50 caracteres',
            'numeric' => 'El campo introducido no es un numero',
            'integer' => 'El stock debe ser un entero mayor que 0',
            'date' => 'El campo tiene que ser una fecha valida (dd-mm-yyyy)',
            'after' => 'La fecha de expiracion debe ser futura',
            'size' => 'La imagen no puede ocupar mas de 2000 Kb'
        );
        $validator = Validator::make($input, $rules, $messages);


        if (isset($input['submit'])) {
            if (isset($input['image'])) {
                if ($input['image']->getClientOriginalExtension() == "jpg" or $input['image']->getClientOriginalExtension() == "jpeg" or $input['image']->getClientOriginalExtension() == "png" or $input['image']->getClientOriginalExtension() == "gif") {
                    $error_extension = false;

                    $img = Image::make($input['image']);
                    $image_size = $img->filesize();

                    if ($image_size > '2097152') {

                        $error_size = true;
                    }
                }else {
                    $error_extension = true;

                }

            }

            if($input['description'] == "<br>" or $input['description'] == "<br> ") {

                $error_description = true;
            }

            if ($input['expiration_date'] != "") {
                $format = explode('-', $input['expiration_date']);
                if (count($format) != 3 ) {
                    $input['expiration_date'] = Product::changeDateFormat($input['expiration_date']);
                }

                if ($input['expiration_date'] > $today) {
                    $error_date = false;
                }else {
                    $error_date = true;
                }

            }

            if (!$error_size and !$error_extension){


                if (isset($input['image']) ) {
                    $extension = $input['image']->getClientOriginalExtension();
                    $image_uploaded = true;
                    $image = $input['image'];
                    $aux_name = 'aux.'.$input['image']->getClientOriginalExtension();

                    $path = public_path('images/tmp/tmp' . $aux_name);
                    $image_path = '/images/tmp/tmp'.$aux_name;
                    $image = Image::make($input['image']->getRealPath());
                    $image->resize(100, 100);
                    Image::make($image)->save($path);
                }

                if ($validator->fails()) {

                    $errors = $validator->messages();
                    session()->flash('message', 'Formulario incompleto o erróneo');
                }else {
                    //mirar intervention
                    //if(isset($input['image'])) {
                        if ($credits - $input['stock'] < 0) {
                            $error_credits = true;
                        }else {

                            $description = str_replace('<br>', " ", $input['description']);
                            unlink("images/tmp/tmpaux.$extension");
                            Product::addProduct($input['title'], $description, $input['price'], $input['stock'], $input['expiration_date'], $input['image'], $user_id);
                            User::changeCredits($credits - $input['stock'], $user_id);

                            $last_prod = Product::getLastProduct();
                            return redirect('/product/'.$last_prod->friendly_url);
                        }
                    //}


                    }
                }
            }
        return view('product.add', compact('input', 'errors', 'error_credits', 'image_path', 'image_uploaded', 'error_description', 'image_path', 'image', 'error_size', 'error_extension', 'extension', 'error_date'));
    }

    public function view($friendly_url) {

        $input = Request::all();

        $correct = Product::checkURl($friendly_url);

        if (!$correct) {
            return view ('error.404');
        }
        $checked = Product::checkOldUrl($friendly_url);
//        if (!$checked) {
//            Header('HTTP/1.1 301 Moved Permanently');
//            Header('Location: ' . 'http://lsgames.local/product/'.$friendly_url);
//            exit;
//        }


        $product_id = Product::getIdByFriendlyURL($friendly_url);
        $user_id = Session::get('user_id');
        $days = Product::getExpirationDate($product_id);
        $stock = Product::getStock($product_id);
        $credits = Session::get('credits');
        $error_credits = false;
        $error_product = false;



        Product::increaseOneVisitor($product_id);
        if ($days > 0 && $stock) {
            $product = Product::getProductInfo($product_id);
            $product['days_lefting'] = $days;
            $user = Product::getSellerInfo($product_id);
            $comments = User::getCommentsFromUser($user['id']);


        }else {

            $error_product = true;
        }

        if (isset($input['submit'])) {
            if ($credits < $product['price']) {
                $error_credits = true;
            }else {


                $this->add_to_buy($product['id']);

                return Redirect::action('UserController@buyCredits');
            }


        }

        return view('product.view', compact('input', 'product', 'user', 'user_id', 'credits', 'error_credits', 'error_product', 'comments'));
    }

    public function my_list ($user_id) {
        $input = Request::all();
        $user_id = Session::get('user_id');

        if (is_null($user_id)) {
            return view ('error.403');
        }

        $products = Product::getAllProductsFromUser($user_id);
        $users = User::paginate(1);

        return view('product.myList', compact('input', 'products'));
    }

    public function delete ($product_id) {
        $user_id = Session::get('user_id');

        if (is_null($user_id)) {
            return view ('error.403');
        }

        Product::deleteProduct($product_id);

        return back()->withInput();
}

    public function edit ($product_id) {

        set_time_limit(0);
        ini_set('memory_limit', '20000M');
        $manager = new ImageManager(array('driver' => 'imagick'));

        $user_id = Session::get('user_id');

        if (is_null($user_id)) {
            return view ('error.403');
        }
        $input = Request::all();

        $credits = Session::get('credits');

        $error_credits = false;
        $error_description = false;
        $image_path = "";
        $image_uploaded = false;
        $error_size = false;
        $error_extension = false;
        $extension = "";

        $error_date = false;
        if (is_null($user_id)) {
            return view ('error.403');
        }

        $form_content = Product::getProductInfo($product_id);

        $today = Carbon::now()->toDateString();
        $rules = array(
            'title' => array('required', 'max:50'),
            'description' => array('required'),
            'price' => array('required','numeric', 'min:0'),
            'stock' => array('required','numeric', 'min:1', 'integer'),
            'expiration_date' => array('required','date', ),
            'image' => array('required')
        );
        $messages = array(
            'required' => 'Este campo es obligatorio',
            'min' => 'El precio debe ser mayor que 0€',
            'max' => 'El título no puede superar los 50 caracteres',
            'numeric' => 'El campo introducido no es un numero',
            'integer' => 'El stock debe ser un entero mayor que 0',
            'date' => 'El campo tiene que ser una fecha valida (dd-mm-yyyy)',
            'after' => 'La fecha de expiracion debe ser futura',
            'size' => 'La imagen no puede ocupar mas de 2000 Kb'
        );
        $validator = Validator::make($input, $rules, $messages);
        $image_path = '/images/products/1-'.$product_id.'.jpg';
        if (isset($input['submit'])) {

            if (isset($input['image'])) {
                if ($input['image']->getClientOriginalExtension() == "jpg" or $input['image']->getClientOriginalExtension() == "jpeg" or $input['image']->getClientOriginalExtension() == "png" or $input['image']->getClientOriginalExtension() == "gif") {
                    $error_extension = false;

                    $img = Image::make($input['image']);
                    $image_size = $img->filesize();

                    if ($image_size > '2097152') {

                        $error_size = true;
                    }
                }else {
                    $error_extension = true;

                }

            }

            if($input['description'] == "<br>" or $input['description'] == "<br> ") {

                $error_description = true;
            }



            if ($input['expiration_date'] != "") {
                $format = explode('-', $input['expiration_date']);
                if (count($format) != 3 ) {
                    $input['expiration_date'] = Product::changeDateFormat($input['expiration_date']);
                }

                if ($input['expiration_date'] > $today) {
                    $error_date = false;
                }else {
                    $error_date = true;
                }

            }
            if (!$error_size and !$error_extension) {

                if ($validator->fails()) {

                    $errors = $validator->messages();
                    session()->flash('message', 'Formulario incompleto o erróneo');
                } else {

                    //mirar intervention
                    if (isset($input['image'])) {


                        if ($credits - $input['stock'] < 0) {
                            $error_credits = true;
                        } else {
                            $description = str_replace('<br>', " ", $input['description']);
                            $input['description'] = $description;

                            Product::updateProduct($product_id, $input);
                            User::changeCredits($credits - $input['stock'], $user_id);

                            return Redirect::action('ProductController@my_list');
                        }
                    }

                }
            }
        }


        return view('product.edit', compact('input', 'form_content', 'user_id', 'error_credits', 'image_path', 'errors', 'error_description', 'image', 'error_size', 'error_extension', 'extension', 'error_date'));
    }

    public function products_list () {
        $input = Request::all();
        $user_id = Session::get('user_id');
        $search_text = Request::input('search_text');
        $error_credits = false;


        $products = Product::getAllProducts($search_text, $user_id);


        $user_id = Session::get('user_id');



        return view('product.products_list', compact('input','products', 'error_credits'));
    }

    public function add_to_buy($product_id) {

        $user_id = Session::get('user_id');
        $buyer_credits = Session::get('credits');

        $product = Product::getProductInfo($product_id);
        if ($product->price > $buyer_credits){

            //$products = Product::getAllProducts(%);
            $error_credits = true;

            return Redirect::action('ProductController@products_list');
        }
        //cambio los creditos del usuario que los ha vendido y le resto 1 al stock
        $seller_credits = User::getCreditsFromUser($product['user_id']);
        $seller_id = $product->user_id;

        User::changeCredits($product['price'] + $seller_credits, $product['user_id']);
        Product::decreaseStock($product_id);

        //ahora los del usuario que los ha comprado
        //$buyer_credits = User::getCreditsFromUser($user_id);

        User::changeCredits($buyer_credits - $product['price'], $user_id);

        Product::setUserPurchase($user_id, $product_id, $seller_id);
        User::increaseSuccessFactor($product['user_id']);

    }



}