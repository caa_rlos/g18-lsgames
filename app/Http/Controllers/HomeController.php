<?php


namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Facades\Session;


class HomeController extends Controller {


    public function index() {

        $user_name = Session::get('name');
        // Desactivamos todo tipo de caches
        header( "Expires: Mon, 20 Dec 1998 01:00:00 GMT" );
        header( "Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT" );
        header( "Cache-Control: no-cache, must-revalidate" );
        header( "Pragma: no-cache" );

        $total_visitors = 0;
        $last_product = Product::getLastProduct();

        $five_last_products = Product::getFiveLastProducts();

        //$popular_products = Product::getPopularProducts();

        $all_visitors = Product::getTotalVisitors();

        foreach ($all_visitors as $visitor) {
            $total_visitors += $visitor->visitors;
        }

//        foreach($popular_products as $product) {
//            $product->count =  Product::getNumberOfSellings($product->id);
//
//        }

        return view('home', compact('last_product', 'five_last_products', 'popular_products', 'total_visitors', 'user_name'));
    }

    public function most_popular_products() {

        $total_visitors = 0;
        $popular_products = Product::getPopularProducts();

        foreach($popular_products as $product) {
            $product->count =  Product::getNumberOfSellings($product->id);

        }

        $all_visitors = Product::getTotalVisitors();

        foreach ($all_visitors as $visitor) {
            $total_visitors += $visitor->visitors;
        }

        return view('most_popular_products', compact( 'popular_products', 'total_visitors'));

    }


}