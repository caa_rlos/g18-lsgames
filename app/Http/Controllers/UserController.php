<?php

namespace App\Http\Controllers;


use App\Product;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;
use Request;
use Auth;


class UserController extends Controller {

    public function register() {

        $input = Request::all();
        $errorTwitter = false;
        $errorName = false;
        $errorEmail = false;
        $input['captcha'] = Input::get('captcha');
        $rules = array(
            'name' => array('required','alpha', 'min:6'),
            'email' => array('required', 'email'),
            'password' => array('required', 'min:6','max:10'),
            'captcha' => array('required', 'captcha')

            );
        $messages = array(
            'min' => 'Este campo debe de tener minimo 6 caracteres',
            'required' => 'Este campo es obligatorio',
            'alpha' => 'El nombre solo debe contener letras, sin espacios',
            'email' => 'El email no es una dirección válida',
            'captcha' => 'El captcha ingresado es incorrecto!'
        );
        $validator = Validator::make($input, $rules, $messages);


        if (isset($input['submit'])) {
            $errorName = User::comprovaNom($input['name']);
            $errorEmail = User::comprovaEmail($input['email']);
            if ($input['twitter'] != "" and $input['twitter'][0] != '@') {

                $errorTwitter = true;
            }else {
                $errorTwitter = false;
            }
            if ($validator->fails()) {

                $errors = $validator->messages();
                session()->flash('message', 'Formulario incompleto o erróneo');
            }else {
                if (!$errorName && !$errorEmail){

                    if ($input['twitter'] == "" || $input['twitter'][0] == '@'){

                        User::insertUser($input['name'], $input['email'], $input['password'], $input['twitter']);
                        $user_id = User::getUserId($input['name']);

                        if (isset($input['image'])){

                            User::addProfilePicture($user_id, $input['image']);

                        }

//                        $this->sendMail();

//                        Mail::send('emails.message', [], function($message)
//                        {
//                            $message->to('carlos.marcelo1992@gmail.com')->subject('it works!');
//                        });


                        $key = User::setActivationCode($input['email']);


                    }else {

                        $errorTwitter = true;

                    }


                }
            }
        }



        return view('user.register', compact('input', 'errors', 'errorTwitter', 'errorName', 'errorEmail', 'key'));
    }

    public function sendMail () {

        //require_once 'vendor/mandrill/mandrill/src/Mandrill.php';
        $mandrill = new \Mandrill('87539fe3f26cebbd4b216f22d2595d96-us13');
        // send transactional email
        try{
            $message = array(
                'subject' => 'Hello, how are you?',
                'text' => 'This is a text message',
                'from_email' => 'info@programarivm.com',
                'to' => array(
                    array(
                        'email' => 'carlos.marcelo1992@gmail.com',
                        'name' => 'info@programarivm.com'
                    )
                )
            );
            $result = $mandrill->messages->send($message);
        } catch(Mandrill_Error $e) {
            echo 'An error occurred while sending this email, please try again or contact us.';
        }

    }

    public function confirm() {
        $hash =$_GET["hash"];

        if (!User::checkCode($hash)){
            $user_id = User::getUserByHash($hash);

            User::activeUser($user_id, $hash);
            User::setSession($user_id);
            return redirect('/');
        } else {

            return view('error');
        }


    }

    public function logout() {

        Session::forget('user_id');
        Session::forget('credits');

        return redirect('/');

    }

    public function login () {

        $input = Request::all();
        $content = "";
        $type = false;
        $error = false;

        if (isset($input['submit'])) {

            $type = $this->check_input($input['text']);
            // true = email; false = name

            if ($type) {
                $id = User::checkUserByEmail($input['text'], $input['password']);

            }else {
                $id = User::checkUserByName($input['text'], $input['password']);

            }

            if (!is_null($id)) {

                User::setSession($id);

                $error = false;
                return redirect('/');
             }else {
                $error = true;
            }
        }


//        $_SESSION['user_id'] = '25';
//        return redirect('/');
        return view('user.login', compact('input', 'content', 'error'));
    }

    public function check_input ($text) {

        if(filter_var($text, FILTER_VALIDATE_EMAIL)) {
            // email
            return true;
        }
        else {
            // name
            return false;
        }
    }

    public function buyCredits () {
        $input = Request::all();

        $my_table = [];


        $actual_credits = Session::get('credits');
        $user_id = Session::get('user_id');

        if (is_null($user_id)) {
            return view ('error.403');
        }
        $my_table = Product::getMyPurchases($user_id);

        $error_quantity = false;

        $rules = array(
            'quantity' => array('required', 'numeric', 'min:1', 'max:1000'),
        );
        $messages = array(
            'required' => 'Este campo es obligatorio',
            'min' => 'Debes añadir mínimo 1 €',
            'max' => 'La cantidad máxima es de 1000 €',
            'numeric' => 'El campo introducido no es un numero'
        );
        $validator = Validator::make($input, $rules, $messages);

        if (isset($input['submit'])) {
            if ($validator->fails()) {

                $errors = $validator->messages();
                session()->flash('message', 'Formulario incompleto o erróneo');
            }else {
                if ($actual_credits + $input['quantity'] > 1000) {
                    $error_quantity = true;
                }else {
                    User::changeCredits($actual_credits + $input['quantity'], $user_id);
                }

            }
        }

        return view ('user.buyCredits', compact('input', 'errors', 'error_quantity', 'actual_credits' ,'my_table'));
    }

    public function user_comments($user_id) {

        $input = Request::all();
        $my_id = Session::get('user_id');
        $error_login = false;
        $condition = false;

        if (is_null($my_id)) {

            $error_login = true;
        }

        $name = User::getUserName($user_id);
        $condition = User::checkConditions($user_id, $my_id);
        $comments = User::getCommentsFromUser($user_id);
        $commented = User::checkIfCommented($my_id, $user_id);

        $my_comment = User::getMyComment($my_id, $user_id);



        if (isset($input['submit'])) {

            if (!$commented) {

                $confirmed = User::addCommentToUser($my_id, $user_id, $input['set_comment']);
                return Redirect::action('UserController@user_comments', [$user_id] );
            }

        }


        return view('user.user_comments', compact('input', 'error_login', 'name', 'condition', 'comments', 'commented', 'my_comment', ''));
    }

    public function delete_comment($comment_id) {

        User::delete_comment($comment_id);

        return Redirect::action('ProductController@products_list');
    }

    public function edit_comment ($comment_id){

        $input = Request::all();
        $comment = User::getCommentById($comment_id);

        if (isset($input['submit'])) {

            User::editComment($comment_id, $input['set_comment']);
            $commented_id = User::getIdFromCommentedUser($comment_id);
            return Redirect::action('UserController@user_comments', array('user_id' => $commented_id->commented_user));
        }

        return view('user.edit_comments', compact('input', 'comment'));
    }

    public function my_comments($user_id) {

        $my_id = Session::get('user_id');

        $all_comments = User::getMyComments($user_id);

        if (is_null($my_id)) {
            return view ('error.403');
        }

        return view('user.my_comments', compact('all_comments'));
    }

    public function validarusuario() {

        $input = Request::all();
//        $name = $_GET['name'];

        $name = $input['name'];
        $user = DB::table('user')->select('id')
            ->where('name', '=',$name)
            ->first();

        if (count($user) != 0) {
            $response = [
                'content' => true
            ];
        }else {
            $response = [
                'content' => false
            ];
        }


        return (new Response(json_encode($response)));
    }

    public function validaremail() {

        $input = Request::all();
//        $name = $_GET['name'];

        $email = $input['email'];
        $user = DB::table('user')->select('id')
            ->where('email', '=',$email)
            ->first();

        if (count($user) != 0) {
            $response = [
                'content' => true
            ];
        }else {
            $response = [
                'content' => false
            ];
        }


        return (new Response(json_encode($response)));
    }

}