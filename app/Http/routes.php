<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('home');
//});



Route::any('/',[
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

Route::any('/most_popular_products/',[
    'as' => 'view',
    'uses' => 'HomeController@most_popular_products'
]);

//User Info
Route::any('/register/',[
    'as' => 'register',
    'uses' => 'UserController@register'
]);

Route::any('/confirm/',[
    'as' => 'confirm',
    'uses' => 'UserController@confirm'
]);

Route::any('/login/',[
    'as' => 'login',
    'uses' => 'UserController@login'
]);

Route::any('/logout/',[
    'as' => 'logout',
    'uses' => 'UserController@logout'
]);

Route::any('/buyCredits/',[
    'as' => 'BuyCredits',
    'uses' => 'UserController@buyCredits'
]);

Route::any('/user_comments/{user_id}',[
    'as' => 'user_comments',
    'uses' => 'UserController@user_comments'
]);

Route::any('/user_comments/delete/{comment_id}',[
    'as' => 'delete_comment',
    'uses' => 'UserController@delete_comment'
]);

Route::any('/user_comments/edit/{comment_id}',[
    'as' => 'edit_comment',
    'uses' => 'UserController@edit_comment'
]);

Route::any('/my_comments/{comment_id}',[
    'as' => 'my_comments',
    'uses' => 'UserController@my_comments'
]);

//Products
Route::any('/product/add',[
    'as' => 'BuyCredits',
    'uses' => 'ProductController@add'
]);


Route::any('/product/myList/{user_id}',[
    'as' => 'myList',
    'uses' => 'ProductController@my_list'
]);

Route::any('/product/delete/{product_id}',[
    'as' => 'delete',
    'uses' => 'ProductController@delete'
]);

Route::any('/product/edit/{product_id}',[
    'as' => 'edit',
    'uses' => 'ProductController@edit'
]);

Route::any('/product/products_list',[
    'as' => 'products_list',
    'uses' => 'ProductController@products_list'
]);

Route::any('/product/add_to_buy/{product_id}',[
    'as' => 'add_to_buy',
    'uses' => 'ProductController@add_to_buy'
]);

Route::any('/product/{friendly_url}',[
    'as' => 'view',
    'uses' => 'ProductController@view'
]);




Route::post('send', ['as' => 'send', 'uses' => 'MailController@send'] );
Route::get('contact', ['as' => 'contact', 'uses' => 'MailController@index'] );

Route::get('/test/datepicker', function () {
    return view('datepicker');
});


Route::post('/validarusuario',[
    'as' => 'validarusuario',
    'uses' => 'UserController@validarusuario'
]);

Route::get('/validarusuario',[
    'as' => 'validarusuario',
    'uses' => 'UserController@validarusuario'
]);


Route::post('/validaremail',[
    'as' => 'validaremail',
    'uses' => 'UserController@validaremail'
]);

Route::get('/validaremail',[
    'as' => 'validaremail',
    'uses' => 'UserController@validaremail'
]);





