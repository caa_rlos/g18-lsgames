<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Image;

class User extends Model {

    protected $table = 'user';

    public static function insertUser($name, $email, $password, $twitter) {

        DB::table('user')->insert(array(
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'twitter' => $twitter,
            'active' => 'N'
        ));
    }

    public static function comprovaNom($name) {
        $user = DB::table('user')->select('id')
             ->where('name', '=',$name)
            ->get();

        if (count($user) != 0) {
            return true;
        }else {
            return false;
        }

    }

    public static function comprovaEmail($email) {

        $user = DB::table('user')->select('id')
            ->where('email', '=',$email
            )->get();

        if (count($user) != 0) {
            return true;
        }else {
            return false;
        }

    }

    public static function setActivationCode($email) {

        $generatedKey = sha1(mt_rand(10000,99999).time().$email);

        $user_id = User::select('id')->where('email', '=',$email)->get();

        DB::table('codes')->insert(array(
            'activation_code' => $generatedKey,
            'user_id' => $user_id[0]->id,
            'used' => false
        ));
        return $generatedKey;
    }

    public static function getUserByHash($hash) {

        $user_id = DB::table('codes')->select('user_id', 'used')
            ->where('activation_code', '=', $hash)
            ->get();

        return $user_id[0]->user_id;


    }

    public static function activeUser($user_id, $hash) {

        User::where('id', '=', $user_id)->update(array('active' => 'Y'));

        DB::table('codes')->where('activation_code', '=', $hash)->update(array('used' => true));
    }

    public static function checkCode ($hash) {

        $used = DB::table('codes')->select('used')->where('activation_code', '=', $hash)->first();

        if ($used->used == 1){
            return true;
        }else {
            return false;
        }
    }

    public static function checkUserByEmail($email, $password) {


        $id = User::select('id')
            ->where('email', '=', $email)
            ->where('password', '=', $password)
            ->where('active', '=', 'Y')
            ->first();
        if (is_null($id)) {
            return null;
        }else {
            return $id->id;
        }

    }

    public static function checkUserByName($name, $password) {


        $id = User::select('id')
            ->where('name', '=', $name)
            ->where('password', '=', $password)
            ->where('active', '=', 'Y')
            ->first();
        if (is_null($id)) {
            return null;
        }else {
            return $id->id;
        }

    }

    public static function setSession ($id) {

        $user = User::select('*')->where('id', '=', $id)->first();
        $_SESSION['user_id'] = $user->id;
        $_SESSION['name'] = $user->name;
        $_SESSION['email'] = $user->email;
        $_SESSION['twitter'] = $user->twitter;
        $_SESSION['credits'] = $user->credits;



        Session::put('user_id', $user->id);
        Session::put('name', $user->name);
        Session::put('email', $user->email);
        Session::put('credits', $user->credits);

    }

    public static function changeCredits($credits, $user_id) {

        User::where('id', '=', $user_id)->
            update(array(
                'credits' => $credits
        ));

        Session::forget('credits');
        Session::put('credits', $credits);
    }

    public static function getCreditsFromUser($user_id) {

        $credits = User::select('credits')->
            where('id', '=', $user_id)
            ->first();

        return $credits->credits;
    }

    public static function increaseSuccessFactor($user_id) {

        $succes_factor = User::select('success_factor')
            ->where('id', '=', $user_id)
            ->first();

        $succes_factor->success_factor += 1;

        User::where('id', '=', $user_id)
            ->update(array(
                'success_factor' => $succes_factor->success_factor
            ));

}

    public static function getUserName($user_id) {

        $name = User::select('name')
            ->where('id', '=', $user_id)
            ->first();

        return $name;

    }

    public static function checkConditions ($commented_id, $user_id){

        $purchases = DB::table('purchases')
            ->select('id')
            ->where('seller_id', '=', $commented_id)
            ->where('user_id', '=', $user_id)
            ->get();

        if (count($purchases) > 0) {
            return true;
        }else {
            return false;

        }
    }

    public static function addCommentToUser($user_id, $commented_user, $comment) {

        return DB::table('comments')
            ->insert(array(
                'commented_user' => $commented_user,
                'user_from' => $user_id,
                'comment' => $comment,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
                ));
    }

    public static function getCommentsFromUser ($user_id) {

        $comments = DB::table('comments as c')
            ->select('c.comment', 'c.user_from', 'u.name', 'c.updated_at', 'u.id')
            ->join('user as u', 'u.id', '=', 'c.user_from')
            ->where('c.commented_user', '=', $user_id)
            ->get();

        return $comments;

    }

    public static function checkIfCommented ($user_id, $commented_user) {

        $comments = DB::table('comments')
            ->select('comment')
            ->where('commented_user', '=', $commented_user)
            ->where('user_from', '=', $user_id)
            ->get();

        if(count($comments) > 0) {
            return true;
        }else {
            return false;
        }
    }

    public static function getMyComment ($user_id, $commented_user) {

        $comments = DB::table('comments')
            ->select('comment', 'id')
            ->where('commented_user', '=', $commented_user)
            ->where('user_from', '=', $user_id)
            ->first();

        return $comments;

    }

    public static function delete_comment ($comment_id)
    {

        DB::table('comments')
            ->where('id', '=', $comment_id)
            ->delete();
    }

    public static function getCommentById ($comment_id) {

        $comment = DB::table('comments')
            ->select('id', 'comment')
            ->where('id', '=', $comment_id)
            ->first();

        return $comment;
    }

    public static function editComment ($comment_id, $new_comment ) {

        DB::table('comments')
            ->where('id', '=', $comment_id)
            ->update(array(
                'comment' => $new_comment,
                'updated_at' => Carbon::now()
            ));
    }

    public static function getIdFromCommentedUser($comment_id) {

        $commented_user = DB::table('comments')
            ->where('id', '=', $comment_id)
            ->select('commented_user')
            ->first();

        return $commented_user;

    }

    public static function getMyComments($user_id) {


        $all_comments = DB::table('comments as c')
            ->join('user as u ', 'u.id', '=', 'c.user_from')
            ->where('c.commented_user', '=', $user_id)
            ->select('c.user_from as commented_id',  'c.comment', 'c.updated_at', 'u.name')
            ->get();





//        $all_comments = DB::table('comments as c')
//            ->join('user as u ', 'u.id', '=', 'c.commented_user')
//            ->where('c.user_from', '=', $user_id)
//            ->select('c.commented_user as commented_id',  'c.comment', 'c.updated_at', 'u.name')
//            ->get();

        return $all_comments;
    }



    public static function getUserId($name) {


        $id = User::select('id')
            ->where('name', '=', $name)
            ->first();
        if (is_null($id)) {
            return null;
        }else {
            return $id->id;
        }

    }

    public static function addProfilePicture($user_id, $image) {


        $filename_path = $image->getClientOriginalName();
        $filename_path = str_replace(' ', '_', $filename_path);
        $destination_path = public_path().'/images/users/';

        $filename = $user_id.'.jpg';

        $path = $destination_path.$filename;

        \Intervention\Image\Facades\Image::make($image->getRealPath())->save($path);



    }




}
