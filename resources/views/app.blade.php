<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Grup 18</title>

    {{--<!-- Fonts -->--}}
    {{--<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">--}}
    <link href="https://www.google.com/fonts#UsePlace:use/Collection:Roboto:400,300,700,500,500italic,700italic,400italic,300italic" type="text/css">

    <!-- Estils -->

    <link rel="stylesheet" href="/css/lsgames.css">
{{--    <link rel="stylesheet" href="{{ elixir("css/app.css")}}">--}}
    {{--<link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">--}}
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    {{--<link href="/css/materialdesignicons.css" rel="stylesheet" type="text/css">--}}
    {{--<link href="/css/styles.css" rel="stylesheet" type="text/css">--}}
    <link type="text/css" rel="stylesheet" href="/css/jquery.qtip.css" />
    <link type="text/css" rel="stylesheet" href="/css/style.css" />

    <link type="text/css" rel="stylesheet" href="/css/header-user-dropdown.css" />
    <link type="text/css" rel="stylesheet" href="/css/footer-basic-centered.css" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
          integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!--      -->

    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />




</head>

<body>

<!-- HEADER -->

<?php $user_id = Session::get('user_id') ?>
<?php $credits = Session::get('credits') ?>
<?php $name = Session::get('name') ?>

@if (isset($user_id))
    <header class="header-user-dropdown">
@else
    <header class="header-user-dropdown" style="height: 300px;">
@endif
    <div class="header-limiter">
                <h1><a href="/">LS<span>Games</span></a></h1>
        @if (isset($user_id))

            <nav>
                <a href="/product/products_list">Productos disponibles</a>
                Fondos:<a href="/buyCredits/"> {{$credits}}</a>
            </nav>

            <div class="header-user-menu" style="z-index: 1;">
                <img src="/images/users/{{$user_id}}.jpg"> </img>
                <ul>
                    <li><a href="/logout">Desconectarse</a></li>
                    <li><a href="/product/add">Añadir producto</a></li>
                    <li><a href="/buyCredits/"> Fondo:{{$credits}}</a></li>
                    <li><a href="/product/myList/{{$user_id}}">Mis productos</a></li>
                    <li><a href="/my_comments/{{$user_id}}">Mis comentarios</a></li>
                </ul>

            </div>

        @else

            <!--<div class="right top-menu" style="text-align:right;">-->
            <div>
                <?php $input = "" ?>
                {!! Form::model($input, array('method' => 'post', 'action' => 'UserController@login', 'class' => 'login' )) !!}
                    <div class="row">
                        {!! Form::label('text','Nombre/ Email: ', ['class' => "black-text"]) !!}
                        {!! Form::input('text', 'text', null, array('class' => 'black-text'))!!}
                    </div>

                    <div class="row">
                        {!! Form::label('password', 'Password', ['class' => "black-text"])!!}
                        {!! Form::input('password', 'password',null, array('class' => 'strong-password black-text'))!!}

                    </div>


                <div class="input-field col m4" style="background-color: #808080;">
                    <a href="/register" class="black-text" >No tienes una cuenta?</a>
                </div>

                <div class="crow">
                    {!! Form::submit('Enviar',['class'=>'btn black-text','type' => 'submit', 'name' => 'submit']) !!}
                </div>

                {!! Form::close() !!}

                <!--<a href="/login">Login</a> -->
            </div>
        @endif
    </div>
@if (isset($user_id))
    </header>
@else
    </header>
@endif

<div class="container content" >
    @yield('content')
</div>

<footer class="footer-basic-centered">

    <p class="footer-company-motto"><h1><a href="/" style="color: #ffffff;">LS<span>Games</span></a></h1></p>

    <p class="footer-company-name">Carlos Marcelo Morales &copy; 2016</p>

</footer>


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
{{--<!--<script type="text/javascript" src="js/materialize.min.js"></script>-->--}}

{{--<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/materialize/0.96.1/js/materialize.min.js"></script>--}}
<script type="text/javascript" src="/js/jquery.sticky.js"></script>
<script type="text/javascript" src="/js/jquery.qtip.min.js"></script>
<!--<script src="js/jquery-1.4.4.min.js" type="text/javascript"></script> -->


<script type="text/javascript"
        src="http://code.jquery.com/jquery-latest.min.js" charset="utf-8">
</script>
<script type="text/javascript" src="/js/pschecker.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>


<!-- WYSWIG -->
<script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

<!-- jquery -->

<script src="/js/jquery-1.12.3.min.js" type="text/javascript"></script>




<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<script>
    @yield('page-script')
</script>


</body>