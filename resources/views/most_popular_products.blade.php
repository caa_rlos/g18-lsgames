@extends('app')

@section('content')
    <div class="container">
        <div class="row"  style="margin-top: 20px;">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Productos más visitados</div>
                    <div class="panel-body">
                        <ul>
                            @foreach($popular_products as $popular_prod)
                                <div class="row">
                                    <li><a href="/product/{{$popular_prod->friendly_url}}">{{$popular_prod->title}}</a>
                                        <label>
                                            Descripción: {{substr($popular_prod->description, 0, 50)}}
                                        </label>
                                        <br>
                                        <label>
                                            Fecha de caducidad: {{$popular_prod->expiration_date}}
                                        </label>
                                        <br>
                                        <label>
                                            Visitantes: {{$popular_prod->visitors}}  ( {{round(($popular_prod->visitors * 100) / $total_visitors, 2)}} %)
                                        </label>
                                        <br>
                                        <label>
                                            Se han vendido {{$popular_prod->count}} unidades.
                                        </label>

                                    </li>

                                </div>
                            @endforeach
                            {!! $popular_products->render() !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection