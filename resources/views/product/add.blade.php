@extends ('app')

@section('content')




    <div class="row title">
        <h1>Añadir nuevo producto</h1>
    </div>
    <div class="form-content row">
        {!! Form::model($input, array('method' => 'post', 'files' => 'true', 'name' => 'AddProduct', 'onsubmit' => 'validateProductForm()', 'class'=>'form-add')) !!}

        <div class="row">
            {!! HTML::decode(Form::label('title','<span style="color: red; font-size: 14px">*</span>Titulo: ')) !!}
            {!! Form::input('text', 'title')!!}
            @if($errors->has('title'))
                <div class="has-error"><span style="color: red;">{{$errors->first('title')}}</span></div>
            @endif
        </div>

        <div class="row">
            {!! HTML::decode(Form::label('description','<span style="color: red; font-size: 14px">*</span>Descripción: ')) !!}
            {!! Form::textarea('description')!!}
            @if($errors->has('description'))
                <div class="has-error"><span style="color: red;">{{$errors->first('description')}}</span></div>
            @endif
            @if ($error_description)
                <div class="has-error"><span style="color: red;">Este campo no puede estar vacio</span></div>
            @endif
        </div>

        <div class="row">
            {!! HTML::decode(Form::label('price','<span style="color: red; font-size: 14px">*</span>Precio: ')) !!}
            {!! Form::input('text', 'price')!!}
            @if($errors->has('price'))
                <div class="has-error"><span style="color: red;">{{$errors->first('price')}}</span></div>
            @endif
        </div>

        <div class="row">
            {!! HTML::decode(Form::label('stock','<span style="color: red; font-size: 14px">*</span>Unidades en stock: ')) !!}
            {!! Form::input('text', 'stock')!!}
            @if($errors->has('stock'))
                <div class="has-error"><span style="color: red;">{{$errors->first('stock')}}</span></div>
            @endif
        </div>

        <div class="row">
            {!! HTML::decode(Form::label('expiration_date','<span style="color: red; font-size: 14px">*</span>Tiempo de expiración: ')) !!}
            {{--{!! Form::input('date', 'last_date', $form_content[0]->last_date, ['class'=>'month_11','id'=>'month_11']) !!}--}}
            {!! Form::input('text', 'expiration_date',null, ['class'=>'datepicker']) !!}

            @if($errors->has('expiration_date'))
                <div class="has-error"><span style="color: red;">{{$errors->first('expiration_date')}}</span></div>
            @endif
            @if($error_date)
                <div class="has-error"><span style="color: red;">La fecha ha de ser futura!</span></div>
            @endif
        </div>

        <div class=" crow ">


            {!! HTML::decode(Form::label('image','<span style="color: red; font-size: 14px">*</span>Imagen: ')) !!}
            {!! Form::file('image', array('id' => 'image', 'style'=> 'display:inline_block')) !!}
            @if($errors->has('image'))
                <div class="has-error"><span style="color: red;">{{$errors->first('image')}}</span></div>
            @endif
            @if($error_size)
                <div class="has-error"><span style="color: red;">La imagen es demasiado grande!</span></div>
            @endif
            @if($error_extension)
                <div class="has-error"><span style="color: red;">La imagen debe ser .png / .jpg / .gif</span></div>
            @endif

            @if ($image_uploaded)
                <img src="/images/tmp/tmpaux.{{$extension}}">
            @endif

        </div>

        @if($error_credits)
            <div class="has-error"><span style="color: red;">No tienes suficientes creditos para añadir tantos productos. </span></div>
        @endif

        <div class="crow">
            {!! Form::submit('Guardar',['class'=>'btn button','type' => 'submit', 'name' => 'submit']) !!}
        </div>

        {!! Form::close() !!}

    </div>

@endsection

@section('page-script')

    $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        language: "es",
        autoclose: true
    });

    function isInt(n) {
        return n % 1 === 0;
    }

    function isFutureDate(idate){
        var today = new Date().getTime(),
        idate = idate.split("/");

        idate = new Date(idate[2], idate[1] - 1, idate[0]).getTime();
        return (today - idate) < 0 ? true : false;
    }

    function validateProductForm() {

    console.log(isFutureDate("2017-03-10"));

        var title = document.forms["AddProduct"]["title"].value;
        var description = document.forms["AddProduct"]["description"].value;
        var price = document.forms["AddProduct"]["price"].value;
        var stock = document.forms["AddProduct"]["stock"].value;
        var expiration_date = document.forms["AddProduct"]["expiration_date"].value;
        var image = document.forms["AddProduct"]["image"].value;

    var now = new Date();


        if (title == null || title == "") {
            alert("El titulo no puede estar vacio");
            return false;
        }
        if (title.length > 50){
            alert("El titulo debe tener menos de 50 caracteres");
            return false;
        }
        if (typeof title != "string") {
            alert("Este campo debe ser un string");
            return false;
        }
        if (description == null || description == "<br>") {
            alert("La descripción no puede estar vacia");
            return false;
        }
        if (typeof description != "string") {
            alert("Este campo debe ser un string");
            return false;
        }
        if (price == null || price == "") {
            alert("El precio no puede estar vacio");
            return false;
        }
        if (price < 0 || price > 9999999999999999999999999999) {
            alert("El precio debe ser un numero entero");
            return false;
        }

        if (!isInt(stock)) {
            alert('El numero de stocks debe ser un numero entero');
            return false;
        }

        if (stock < 0) {
            alert('El stock debe ser un numero mayor que 0');
        return false;
        }
        if(!isFutureDate(expiration_date)){
            alert('La fecha seleccionada no es futura.');
            return false;
        }

        ValidateFileUpload();
    }

    function ValidateFileUpload() {
        var fuData = document.getElementById('image');
        var FileUploadPath = fuData.value;

        //To check if user upload any file
        if (FileUploadPath == '') {
            alert("Por favor introduce una imagen");

        } else {
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image

        if (Extension == "gif" || Extension == "png" || Extension == "jpeg" || Extension == "jpg") {

            // To Display
            if (fuData.files && fuData.files[0]) {

                var size = fuData.files[0].size;



                if(size > 2097152){
                    alert("La imagen no puede ser mayor que 2 MB");
                    return;
                }else{

                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                }
            }

        }

        //The file upload is NOT an image
        else {
            alert("La imagen debe ser .png / .jpg / .gif ");

        }}
    }

@endsection