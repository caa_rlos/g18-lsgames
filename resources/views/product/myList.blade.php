@extends('app')

@section('content')


    <div clas="row title"><h1>Mis productos</h1></div>

    <div class="row">
        <table>
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($products as $product)
                <tr>
                    <td>
                        <a href="/product/{{$product->friendly_url}}">{{$product->title}}</a>
                    </td>
                    <td>
                        <a href="/product/edit/{{$product->id}}">Editar</a>
                        <a class="delete" href="/product/delete/{{$product->id}}">Borrar</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

    {!! $products->render() !!}


@endsection

@section('page-script')

    $(".delete").click(function(){
    return window.confirm("Está seguro que desea borrar este producto?");
    });

@stop