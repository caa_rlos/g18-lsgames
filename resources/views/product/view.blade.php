@extends ('app')

@section('content')

    <div><a href="{{ URL::previous() }}">Atras</a></div>

    @if (!$error_product)
        <div class="row title"><h1>{{$product['title']}}</h1></div>

        <div class="row">
            <label>
                Descripción: {{$product['description']}}
            </label>
        </div>

        <div class="row">
            <label>
                Dias restantes: {{$product['days_lefting']}}
            </label>
        </div>

        <div class="row">
            <label>
                Precio por unidad: {{$product['price']}} €
            </label>
        </div>

        <div class="row">
            <label>
                Cantidad disponible: {{$product['stock']}}
            </label>
        </div>

        <div class="row">
            <label>
                Imagen: <img src="/images/products/1-{{$product['id']}}.jpg">
            </label>
        </div>

        <div class="row">
            <label>
                Vendedor: <img width="30" height="30" src="/images/users/{{$user['id']}}.jpg" >{{$user['name']}} ({{$user['success_factor']}})

                @if ($user['success_factor'] >= 0 && $user['success_factor'] < 25)
                    <img src="/images/star.jpg">
                @endif

                @if ($user['success_factor'] >= 25 && $user['success_factor'] < 50)
                    <img src="/images/star.jpg">
                    <img src="/images/star.jpg">
                @endif

                @if ($user['success_factor'] >= 50 && $user['success_factor'] < 75)
                    <img src="/images/star.jpg">
                    <img src="/images/star.jpg">
                    <img src="/images/star.jpg">
                @endif

                @if ($user['success_factor'] >= 75 && $user['success_factor'] < 100)
                    <img src="/images/star.jpg">
                    <img src="/images/star.jpg">
                    <img src="/images/star.jpg">
                    <img src="/images/star.jpg">
                @endif

                @if ($user['success_factor'] >= 100)
                    <img src="/images/star.jpg">
                    <img src="/images/star.jpg">
                    <img src="/images/star.jpg">
                    <img src="/images/star.jpg">
                    <img src="/images/star.jpg">
                @endif

            </label>
        </div>
        <div class="row">
            <label>
                Visitantes: {{$product['visitors']}}
            </label>
        </div>
        <div class="comments">
            <h4>Comentarios sobre {{$user['name']}}</h4>
            <ul>
                @foreach ($comments as $comment)
                    <li>{{$comment->name}} : {{$comment->comment}}</li>
                @endforeach
            </ul>
        </div>
        <div class="row">
            @if ($product['user_id'] != $user_id )
                {{--<a href="product/add_to_buy/{{$product['product_id']}}">Comprar</a>--}}

                <div class="form-content row">
                    {!! Form::model($input, array('method' => 'post', 'files' => 'true', 'name' => 'AddProduct', 'onsubmit' => 'validateProductForm()')) !!}
<?php
                    $user_id = Session::get('user_id');
                    ?>


                @if (!is_null($user_id))
                    <div class="crow">
                        {!! Form::submit('Comprar',['class'=>'btn','type' => 'submit', 'name' => 'submit']) !!}
                    </div>
                    @if($error_credits)
                        <div class="has-error"><span style="color: red;">No dispones de suficientes creditos para comprar este producto!</span></div>
                    @endif
                @endif
                    {!! Form::close() !!}
                </div>

            @endif
        </div>


    @else

        <h1>Lo sentimos, este producto ya no está disponible.</h1>
    @endif


@endsection