@extends ('app')

@section('content')

   <div class="row title"><h1> Productos disponibles</h1></div>


   <div class="row week-filter">

       {!! Form::model($input, array('method' => 'post', 'class' => 'form')) !!}
       <div class="col m4">
           {!! Form::input('text', 'search_text', null, ['id'=>'search_text']) !!}
       </div>

       <div class="col m2">
           {!! Form::submit('Buscar',['class'=>'btn button','type' => 'submit','name' => 'submit']) !!}
       </div>
       {!! Form::close() !!}

       @if(session()->has('message'))
           {{ session()->get('message') }}
       @endif
   </div>

   @if ($error_credits)
       <div class="has-error"><span style="color: red;">No dispones de suficientes creditos para comprar este producto</span></div>

   @endif

   <div class="row">
       <div class="col m4">
           <table>
               <thead>
               <tr>
                   <th>Nombre</th>
                   <th>Vendedor</th>
                   <th>Factor de éxito</th>
                   {{--<th>Acciones</th>--}}
               </tr>
               </thead>
               <tbody>
               @foreach ($products as $product)
                   <tr>
                       <td>
                           <a href="/product/{{$product->friendly_url}}">{{$product->title}}</a>
                       </td>
                       <td>
                           <img width="50" height="50" src="/images/users/{{$product->user_id}}.jpg"/><a href="/user_comments/{{$product->user_id}}">{{$product->name}}</a>
                       </td>
                        <td>
                            {{$product->success_factor}}
                        </td>
                       {{--<td>--}}
                           {{--<a class="buy" href="/product/add_to_buy/{{$product->id}}">Comprar</a>--}}
                       {{--</td>--}}
                   </tr>
               @endforeach
               </tbody>
           </table>
           {!! $products->render() !!}
       </div>

   </div>

   {{--{!! $products->render() !!}--}}
@endsection

@section('page-script')

    $(".buy").click(function(){
    return window.confirm("Seguro que quieres comprar este producto?");
    });

@stop