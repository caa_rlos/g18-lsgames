@extends('app')

@section ('content')

    <div><a href="/product/myList/{{$user_id}}">Atrás</a></div>

    <div class="row title">
        <h1>Editar producto</h1>
    </div>
    <div class="form-content row">
        {!! Form::model($input, array('method' => 'post', 'files' => 'true', 'name' => 'AddProduct', 'onsubmit' => 'validateProductForm()', 'class'=>'form-add')) !!}

        <div class="row">
            {!! HTML::decode(Form::label('title','<span style="color: red; font-size: 14px">*</span>Titulo: ')) !!}
            {!! Form::input('text', 'title', $form_content->title)!!}
            @if($errors->has('title'))
                <div class="has-error"><span style="color: red;">{{$errors->first('title')}}</span></div>
            @endif
        </div>

        <div class="row">
            {!! HTML::decode(Form::label('description','<span style="color: red; font-size: 14px">*</span>Descripción: ')) !!}
            {!! Form::textarea('description',  $form_content->description)!!}
            @if($errors->has('description'))
                <div class="has-error"><span style="color: red;">{{$errors->first('description')}}</span></div>
            @endif
            @if ($error_description)
                <div class="has-error"><span style="color: red;">Este campo no puede estar vacio</span></div>
            @endif
        </div>

        <div class="row">
            {!! HTML::decode(Form::label('price','<span style="color: red; font-size: 14px">*</span>Precio: ')) !!}
            {!! Form::input('text', 'price',  $form_content->price)!!}
            @if($errors->has('price'))
                <div class="has-error"><span style="color: red;">{{$errors->first('price')}}</span></div>
            @endif
        </div>

        <div class="row">
            {!! HTML::decode(Form::label('stock','<span style="color: red; font-size: 14px">*</span>Unidades en stock: ')) !!}
            {!! Form::input('text', 'stock',  $form_content->stock)!!}
            @if($errors->has('stock'))
                <div class="has-error"><span style="color: red;">{{$errors->first('stock')}}</span></div>
            @endif
        </div>

        <div class="row">
            {!! HTML::decode(Form::label('expiration_date','<span style="color: red; font-size: 14px">*</span>Tiempo de expiración:')) !!}
            {{--{!! Form::input('date', 'last_date', $form_content[0]->last_date, ['class'=>'month_11','id'=>'month_11']) !!}--}}
            {!! Form::input('date', 'expiration_date', $form_content->expiration_date, ['class'=>'datepicker','id'=>'expiration_date']) !!}
            @if($errors->has('expiration_date'))
                <div class="has-error"><span style="color: red;">{{$errors->first('expiration_date')}}</span></div>
            @endif
            @if($error_date)
                <div class="has-error"><span style="color: red;">La fecha ha de ser futura!</span></div>
            @endif
        </div>

        <div class=" crow ">
            {!! HTML::decode(Form::label('image','<span style="color: red; font-size: 14px">*</span>Imagen: ')) !!}
            {!! Form::file('image', array('id' => 'image', 'style'=> 'display:inline_block')) !!}
            @if($errors->has('image'))
                <div class="has-error"><span style="color: red;">{{$errors->first('image')}}</span></div>
            @endif
            @if($error_size)
                <div class="has-error"><span style="color: red;">La imagen es demasiado grande!</span></div>
            @endif
            @if($error_extension)
                <div class="has-error"><span style="color: red;">La imagen debe ser .png / .jpg / .gif</span></div>
            @endif
                <div class="row">
                    Imagen actual
                    <img src="{{$image_path}}">
                </div>


        </div>

        @if($error_credits)
            <div class="has-error"><span style="color: red;">No tienes suficientes creditos para añadir tantos productos. </span></div>
        @endif

        <div class="crow">
            {!! Form::submit('Guardar',['class'=>'btn button','type' => 'submit', 'name' => 'submit']) !!}
        </div>

        {!! Form::close() !!}

    </div>

@endsection

@section ('page-script')

    $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        language: "es",
        autoclose: true
    });

@endsection