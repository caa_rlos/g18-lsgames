@extends('app')

@section('content')
<div class="container">
    <?php
    $user_id = Session::get('user_id');


    ?>
    @if (!is_null($user_id))
        <h1>Bienvenido, {{$user_name}}</h1>
    @endif
	<div class="row" style="margin-top: 20px;">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Último producto a la venta!</div>
				<div class="panel-body">
                    <a href="/product/{{$last_product->friendly_url}}"><h2>{{$last_product->title}}</h2></a>
                    <label style>
                        <img src="/images/products/1-{{$last_product['id']}}.jpg">
                    </label>
				</div>
			</div>
		</div>
	</div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Últimos productos</div>
                <div class="panel-body">
                    @foreach($five_last_products as $prod)
{{--                            {{dd($five_last_products)}}--}}
                        <div class="row">

                                <label style>
                                    <img src="/images/products/2-{{$prod->id}}.jpg">
                                </label>

                                <label>
                                    Titulo: <a href="/product/{{$prod->friendly_url}}">{{$prod->title}}</a>
                                </label>
                                <br>
                                <label>
                                    Descripción: {{substr($prod->description, 0, 50)}}
                                </label>
                                <br>
                                <label>
                                    Fecha de caducidad: {{$prod->expiration_date}}
                                </label>
                                <br>
                                <label>
                                    Precio de venda: {{$prod->price}} €
                                </label>
                        </div>

                        _________________________________________________________________________________________________________

                    @endforeach

                </div>
                <a href="/most_popular_products">Ver más!</a>


            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Últimos Hashtags #LSStore</div>
                <div class="panel-body" style="text-align:center">
                    <a class="twitter-timeline"  href="https://twitter.com/hashtag/LSStore" data-widget-id="731519929976016896">Tweets sobre #LSStore</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
