@extends ('app')

@section('content')

    <div class="row">
        <div class="col m4">
            <table>
                <thead>
                <tr>
                    <th>Comentario</th>
                    <th>Usuario</th>
                    <th>Fecha</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($all_comments as $comment)
                    <tr>
                        <td>
                            {{$comment->comment}}
                        </td>
                        <td>
                            <a href="/user_comments/{{$comment->commented_id}}">{{$comment->name}}</a>
                        </td>
                        <td>
                            {{$comment->updated_at}}
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection