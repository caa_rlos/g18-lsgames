@extends('app')

@section ('content')
    <div class="row title"><h1>Compra de creditos</h1></div>

    <div class="form-content">
        {!! Form::model($input, array('method' => 'post' , 'class' => 'login' )) !!}

        <div class="row">
            {!! Form::label('quantity', 'Cantidad')!!}
            {!! Form::input('text', 'quantity')!!}
        </div>
        @if($errors->has('quantity'))
            <div class="has-error"><span style="color: red;">{{$errors->first('quantity')}}</span></div>
        @endif
        @if ($error_quantity)
            <div class="has-error"><span style="color: red;">Tu cantidad de dinero no puede pasar de los 1000€</span></div>

        @endif

        <div class="row">
            {!! Form::label('type', 'Método:')!!}
            {!! Form::select('type', [0 => 'Visa',1 => 'PayPal' ,2 => 'Transferencia'], null)  !!}
        </div>

        <div class="crow">
            {!! Form::submit('Guardar',['class'=>'btn','type' => 'submit', 'name' => 'submit']) !!}
        </div>

        {!! Form::close() !!}
    </div>

    <div class="col m4">
        <h1 class="title">Mis compras</h1>
        <table>
            <thead>
            <tr>
                <th>Producto</th>
                <th>Precio</th>
                <th>Vendedor</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($my_table as $product)
                <tr>
                    <td>
                        <a href="/product/{{$product->friendly_url}}">{{$product->title}}</a>
                    </td>
                    <td>
                        {{$product->price}}
                    </td>
                    <td>
                        <a href="/user_comments/{{$product->user_id}}">{{$product->name}}</a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection()