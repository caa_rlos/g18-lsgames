@extends ('app')

@section('content')

    <div class="content">

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar comentario </div>
                    <div class="panel-body">
                        <div class="form-content row">

                            {!! Form::model($input, array('method' => 'post', 'files' => 'true', 'class' => 'form-add')) !!}

                            {!! Form::input('text', 'set_comment', $comment->comment, array('class' => 'edit-comment'))!!}

                            <div class="crow">
                                {!! Form::submit('Guardar',['class'=>'btn button','type' => 'submit', 'name' => 'submit']) !!}
                            </div>
                            {!! Form::close()!!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection