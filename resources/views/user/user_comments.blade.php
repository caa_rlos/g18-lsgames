@extends ('app')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Comentarios sobre {{$name->name}}</div>
                    <div class="panel-body">

                        <div>
                            @foreach($comments as $comment)
                                <p><a href="/user_comments/{{$comment->id}}">{{$comment->name}}</a>: {{$comment->comment}}   ({{$comment->updated_at}})</p>

                            @endforeach
                        </div>

                        <div class="form-content row">
                            {!! Form::model($input, array('method' => 'post', 'files' => 'true', 'class' => 'form-add')) !!}


                            @if (!$error_login && $condition && !$commented)
                                <div class="row">
                                    {!! Form::input('text', 'set_comment', null, array('class' => 'edit-comment'))!!}
                                </div>


                                <div class="crow">
                                    {!! Form::submit('Guardar',['class'=>'btn button','type' => 'submit', 'name' => 'submit']) !!}
                                </div>

                                {!! Form::close() !!}
                            @else
                                @if ($error_login)
                                    <p>Para comentar debes estar logueado! <a href="/register">Registrate</a> | <a href="/login">Login</a></p>
                                @elseif (!$condition)
                                    <p>No puedes comentar sin comprar un producto de este vendedor antes!</p>
                                @elseif ($commented)
                                    <p>Ya has puesto un comentario acerca de este usuario.</p>
                                @endif
                            @endif

                        </div>

                        <div>
                            @if ($commented)
                                <div class="row">
                                    <p> Tu comentario:</p>
                                     {{$my_comment->comment}}
                                </div>

                                <a href="/user_comments/edit/{{$my_comment->id}}">Editar</a>  |  <a class="delete" href="/user_comments/delete/{{$my_comment->id}}">Eliminar</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page-script')

    $(".delete").click(function(){
    return window.confirm("Seguro que quieres eliminar este comentario?");
    });

@stop