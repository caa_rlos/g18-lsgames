@extends('app')
@section('content')

    <div class="row title">
        <h1>Registro de usuario</h1>
    </div>

    <div class="row form-content">
        {!! Form::model($input, array('method' => 'post', 'files' => 'false', 'name' => 'UserForm', 'onsubmit' => 'validateForm()')) !!}
        <div class="row">
            {!! HTML::decode(Form::label('name','<span style="color: red; font-size: 14px">*</span>Nombre: ')) !!}
            {!! Form::input('text', 'name', null, array('id' => 'username'))!!}
            @if($errors->has('name'))
                <div class="has-error"><span style="color: red;">{{$errors->first('name')}}</span></div>
            @endif
            @if ($errorName)
                <div class="has-error"><span style="color: red;">Ya existe este usuario!</span></div>
            @endif
        </div>


        <div class="row">
            {!!  HTML::decode(Form::label('email', '<span style="color: red; font-size: 14px">*</span>Email'))!!}
            {!! Form::input('text', 'email')!!}
            @if($errors->has('email'))
                <div class="has-error"><span style="color: red;">{{$errors->first('email')}}</span></div>
            @endif
            @if ($errorEmail)
                <div class="has-error"><span style="color: red;">Ya existe un usuario con este email!</span></div>
            @endif
        </div>

        <div class="password-container">
            {!! HTML::decode(Form::label('password', '<span style="color: red; font-size: 14px">*</span>Password'))!!}
            {!! Form::input('password', 'password',null, array('class' => 'strong-password'))!!}
            @if($errors->has('password'))
                <div class="has-error"><span style="color: red;">{{$errors->first('password')}}</span></div>
            @endif
            <div class="strength-indicator">
                <div class="meter">
                </div>
                Strong passwords contain 8-16 characters, do not include common words or names,
                and combine uppercase letters, lowercase letters, numbers, and symbols.
            </div>
        </div>

        <div class="row">
            {!! Form::label('twitter', 'Twitter')!!}
            {!! Form::input('text', 'twitter')!!}
            @if($errorTwitter)
                <div class="has-error"><span style="color: red;">Este campo debe empezar por @</span></div>
            @endif
        </div>

        <div class=" crow ">
            {!! Form::label('image', 'Foto de perfil:', ['class' => "set-for-label"]) !!}
            {!! Form::file('image', array('id' => 'image', 'style'=> 'display:inline_block' )) !!}
        </div>


        <div class="captcha">

            <p>{!! Captcha::img(); !!}</p>
            <p>{!! Form::text('captcha') !!}</p>
        </div>
        @if($errors->has('captcha'))

                <div class="has-error"><span style="color: red;">{{$errors->first('captcha')}} </span></div>
        @endif

        <div class="input-field col m4">
            <a href="{{ URL::previous() }}">Cancelar</a>
        </div>

        <div class="crow">
            {!! Form::submit('Guardar',['class'=>'btn button','type' => 'submit', 'name' => 'submit']) !!}
        </div>

        {!! Form::close() !!}

        @if (isset($key))
            <a href="/confirm?hash={{$key}}">{{$key}}</a>

        @endif

    </div>


@endsection

@section('page-script')

    (function ($) {
    $.fn.extend({
    pschecker: function (options) {
    var settings = $.extend({ minlength: 8, maxlength: 16, onPasswordValidate: null, onPasswordMatch: null }, options);
    return this.each(function () {
    var wrapper = $('.password-container');
    var password = $('.strong-password:eq(0)', wrapper);
    var cPassword = $('.strong-password:eq(1)', wrapper);

    cPassword.removeClass('no-match');
    password.keyup(validatePassword).blur(validatePassword).focus(validatePassword);
    cPassword.keyup(validatePassword).blur(validatePassword).focus(validatePassword);

    function validatePassword() {
    var pstr = password.val().toString();
    var meter = $('.meter');
    meter.html("");
    //fires password validate event if password meets the min length requirement
    if (settings.onPasswordValidate != null)
    settings.onPasswordValidate(pstr.length >= settings.minlength);

    if (pstr.length < settings.maxlength)
    meter.removeClass('strong').removeClass('medium').removeClass('week');
    if (pstr.length > 0) {
    var rx = new RegExp(/^(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{7,30}$/);
    if (rx.test(pstr)) {
    meter.addClass('strong');
    meter.html("Strong");
    }
    else {
    var alpha = containsAlpha(pstr);
    var number = containsNumeric(pstr);
    var upper = containsUpperCase(pstr);
    var special = containsSpecialCharacter(pstr);
    var result = alpha + number + upper + special;

    if (result > 2) {
    meter.addClass('medium');
    meter.html("Medium");
    }
    else {
    meter.addClass('week');
    meter.html("Week");
    }
    }
    if (cPassword.val().toString().length > 0) {
    if (pstr == cPassword.val().toString()) {
    cPassword.removeClass('no-match');
    if (settings.onPasswordMatch != null)
    settings.onPasswordMatch(true);
    }
    else {
    cPassword.addClass('no-match');
    if (settings.onPasswordMatch != null)
    settings.onPasswordMatch(false);
    }
    }
    else {
    cPassword.addClass('no-match');
    if (settings.onPasswordMatch != null)
    settings.onPasswordMatch(false);
    }
    }
    }

    function containsAlpha(str) {
    var rx = new RegExp(/[a-z]/);
    if (rx.test(str)) return 1;
    return 0;
    }

    function containsNumeric(str) {
    var rx = new RegExp(/[0-9]/);
    if (rx.test(str)) return 1;
    return 0;
    }

    function containsUpperCase(str) {
    var rx = new RegExp(/[A-Z]/);
    if (rx.test(str)) return 1;
    return 0;
    }
    function containsSpecialCharacter(str) {

    var rx = new RegExp(/[\W]/);
    if (rx.test(str)) return 1;
    return 0;
    }


    });
    }
    });
    })(jQuery);


    $(document).ready(function () {

    //Demo code
    $('.password-container').pschecker({ onPasswordValidate: validatePassword, onPasswordMatch: matchPassword });

    var submitbutton = $('.submit-button');
    var errorBox = $('.error');
    errorBox.css('visibility', 'hidden');
    submitbutton.attr("disabled", "disabled");

    //this function will handle onPasswordValidate callback, which mererly checks the password against minimum length
    function validatePassword(isValid) {
    if (!isValid)
    errorBox.css('visibility', 'visible');
    else
    errorBox.css('visibility', 'hidden');
    }
    //this function will be called when both passwords match
    function matchPassword(isMatched) {
    if (isMatched) {
    submitbutton.addClass('unlocked').removeClass('locked');
    submitbutton.removeAttr("disabled", "disabled");
    }
    else {
    submitbutton.attr("disabled", "disabled");
    submitbutton.addClass('locked').removeClass('unlocked');
    }
    }
    });



    function validateForm() {

        var name = document.forms["UserForm"]["name"].value;

        {{--validateUsername(name);--}}
        var email = document.forms["UserForm"]["email"].value;
        {{--validateEmail(email);--}}
        var pass = document.forms["UserForm"]["password"].value;
        var twitter = document.forms["UserForm"]["twitter"].value;

        var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (name == null || name == "") {
            alert("El nombre no puede estar vacio");
            return false;
        }
        if (name.length < 6){
            alert("El nombre debe tener mínimo 6 caracteres");
            return false;
        }

        if (email == null || email == "") {
            alert("El email no puede estar vacio!");
        }
        if (!filter.test(email)) {
            alert("El email es incorrecto!");
            return false;
        }

        if ((pass.length < 6) || (pass.length > 10)) {
            alert("La contraseña debe tener entre 6 y 10 caracteres");
            return false;
        }

        if (twitter != "" && twitter[0] != "@") {
            alert("El campo de twitter debe empezar por '@'");
        }

    }


    function validateUsername(name) {

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: './validarusuario',
            data: {
                '_token': CSRF_TOKEN,
                'name': name
            },
            type: 'post',
            dataType: 'JSON',
            success: function(content){
                console.log(content);
                if(content.content == true){
                    alert('ya existe este usuario');
                }

            }
        });

    }

    function validateEmail(email) {

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: './validaremail',
            data: {
                '_token': CSRF_TOKEN,
                'email': email
            },
            type: 'post',
            dataType: 'JSON',
            success: function(content){
                console.log(content);
                if(content.content == true){
                    alert('ya existe un usuario con este email');
                }

            }
        });
    }












@endsection